package ch.zhaw.rest;

import ch.zhaw.model.Measurement;
import io.micronaut.http.HttpResponse;
import io.micronaut.http.MediaType;
import io.micronaut.http.annotation.Body;
import io.micronaut.http.annotation.Controller;
import io.micronaut.http.annotation.Post;

@Controller("/measurements")
public class MeasurementsController {

    @Post(value = "/", consumes = MediaType.APPLICATION_JSON, produces = MediaType.APPLICATION_JSON)
    public HttpResponse post(@Body Measurement measurement) {
        return HttpResponse.created(measurement);
    }

}