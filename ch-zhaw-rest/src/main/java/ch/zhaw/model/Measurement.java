package ch.zhaw.model;

import java.time.ZonedDateTime;

public class Measurement {

    ZonedDateTime dateTime;
    int value;

    public Measurement() {
    }

    public ZonedDateTime getDateTime() {
        return dateTime;
    }

    public void setDateTime(ZonedDateTime dateTime) {
        this.dateTime = dateTime;
    }

    public int getValue() {
        return value;
    }

    public void setValue(int value) {
        this.value = value;
    }
}
