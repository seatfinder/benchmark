package ch.zhaw.rabbitmq;

import ch.zhaw.rabbitmq.model.Measurement;
import io.micronaut.configuration.rabbitmq.annotation.Queue;
import io.micronaut.configuration.rabbitmq.annotation.RabbitListener;

@RabbitListener
public class MeasurementsListener {

    @Queue("measurements")
    public Measurement receive(Measurement m){
        return m;
    }
}