package ch.zhaw.grpc;

import io.grpc.stub.StreamObserver;

import javax.inject.Singleton;
import java.time.Instant;
import java.time.ZoneId;
import java.time.ZonedDateTime;

@Singleton
public class MeasurementsService extends MeasurementsServiceGrpc.MeasurementsServiceImplBase {

    private final ZoneId zone = ZoneId.of("Europe/Zurich");

    @Override
    public void send(MeasurementsRequest request, StreamObserver<MeasurementsReply> responseObserver) {
        Instant instant = Instant.now();
        ZonedDateTime dateTime = instant.atZone(zone);
        MeasurementsReply reply = MeasurementsReply.newBuilder()
                .setDateTime(dateTime.toString())
                .setValue(request.getValue())
                .build();
        responseObserver.onNext(reply);
        responseObserver.onCompleted();
    }
}