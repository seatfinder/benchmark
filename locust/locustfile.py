import json
import os
import subprocess
import sys
from os.path import dirname
from urllib.parse import urlparse

import grpc
import pika
from google.protobuf.timestamp_pb2 import Timestamp
from locust import HttpLocust, TaskSet, task, constant, Locust, events

from common.common import DateTimeEncoder, StopWatch
from common.sensor import Sensor
from common.service import Service

sys.path.append(os.path.join(dirname(__file__), 'grpc_stubs'))
import measurements_pb2_grpc
from measurements_pb2 import MeasurementsRequest

REST_LOCALHOST = "http://localhost:8080"
GRPC_LOCALHOST = "grpc://localhost:50051"
AMQP_LOCALHOST = "amqp://localhost:5672"


class Http_Tasks(TaskSet):

    def on_start(self):
        self.sensor = Sensor()

    @task
    def post(self):
        m = self.sensor.measurement()
        m["dateTime"] = m["dateTime"].isoformat()
        self.client.post("/measurements", json=m, name="/measurements")


class REST(HttpLocust):
    task_set = Http_Tasks
    host = REST_LOCALHOST
    wait_time = constant(0)
    service = Service("../ch-zhaw-rest")

    def setup(self):
        self.service.start()

    def teardown(self):
        self.service.stop()


class Protobuf_Tasks(TaskSet):
    def on_start(self):
        self.sensor = Sensor()
        self.watch = StopWatch()

    @task
    def send(self):
        data = self.sensor.measurement()
        t = Timestamp().FromDatetime(dt=data["dateTime"])
        request = MeasurementsRequest(dateTime=t, value=data["value"])
        try:
            self.watch.start()
            response = self.client.send(request)
        except Exception as e:
            self.watch.stop()
            events.request_failure.fire(request_type="protobuf", name="MeasurementsService",
                                        response_time=self.watch.elapsed_time(),
                                        response_length=0,
                                        exception=e)
        else:
            self.watch.stop()
            events.request_success.fire(request_type="protobuf", name="MeasurementsService",
                                        response_time=self.watch.elapsed_time(),
                                        response_length=0)


class GrpcLocust(Locust):

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        try:
            self.channel = grpc.insecure_channel(self.host.netloc)
            self.client = measurements_pb2_grpc.MeasurementsServiceStub(self.channel)
        except Exception as e:
            print(sys.exc_info())
            raise RuntimeError("Error when trying to create channel/Service Stub:" + self.host.netloc)

    def teardown(self):
        self.channel.close()


class GRPC(GrpcLocust):
    task_set = Protobuf_Tasks
    host = urlparse(GRPC_LOCALHOST)
    wait_time = constant(0)
    service = Service("../ch-zhaw-grpc")

    def setup(self):
        self.service.start()

    def teardown(self):
        self.service.stop()


class Amqp_Tasks(TaskSet):

    def on_start(self):
        self.sensor = Sensor()
        self.watch = StopWatch()

    @task
    def publish(self):
        data = self.sensor.measurement()
        message = json.dumps(data, cls=DateTimeEncoder)
        try:
            self.watch.start()
            response = self.client.basic_publish(exchange="micronaut", routing_key="micronaut.measurements",
                                                 body=message)
        except Exception as e:
            self.watch.stop()
            events.request_failure.fire(request_type="amqp", name="micronaut.measurements",
                                        response_time=self.watch.elapsed_time(),
                                        exception=e)
        else:
            self.watch.stop()
            events.request_success.fire(request_type="amqp", name="micronaut.measurements",
                                        response_time=self.watch.elapsed_time(),
                                        response_length=0)


class RabbitMQLocust(Locust):

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        try:
            self.connection = pika.BlockingConnection(pika.URLParameters(self.host.geturl()))
            self.client = self.connection.channel()
        except Exception as e:
            print(sys.exc_info())
            raise RuntimeError("Error when trying to connect to: " + self.host.geturl())

    def teardown(self):
        self.client.close()
        self.connection.close()


class RabbitMQ(RabbitMQLocust):
    task_set = Amqp_Tasks
    host = urlparse(AMQP_LOCALHOST)
    wait_time = constant(0)
    service = Service("../ch-zhaw-rabbitmq")

    @staticmethod
    def Broker(command):
        commands = {
            "start": "rabbitmq-server -detached",
            "stop": "rabbitmqctl stop"}
        to_exec = commands[command]
        retcode = subprocess.call(to_exec, timeout=5, shell=True, close_fds=True)
        if retcode != 0:
            raise RuntimeError("Error when trying to execute: " + to_exec)

    def setup(self):
        RabbitMQ.Broker("start")
        self.service.start()

    def teardown(self):
        self.service.stop()
        RabbitMQ.Broker("stop")
