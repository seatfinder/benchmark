#!/bin/zsh

python -m grpc_tools.protoc \
  -I../ch-zhaw-grpc/src/main/proto \
  --python_out=grpc_stubs/ \
  --grpc_python_out=grpc_stubs/ \
  ../ch-zhaw-grpc/src/main/proto/measurements.proto