#!/bin/zsh

ulimit -a

## one process being used
locust --no-web --run-time 3m --csv=results/REST --only-summary --logfile results/REST REST
locust --no-web --run-time 3m --csv=results/GRPC --only-summary --logfile results/GRPC GRPC
locust --no-web --run-time 3m --csv=results/RabbitMQ --only-summary --logfile results/RabbitMQ RabbitMQ

## >1 processes being used
#protocols=('REST' 'GRPC' 'RabbitMQ')
#export countSlaves=4

## pseudocode
#for p in $protocols; do
#  locust --no-web --master --expect-slaves=$countSlaves --run-time 3m --csv=results/$p --only-summary --logfile results/$p $p
  ## in other zsh session
  #for ((i = 0; i < countSlaves; i++)); do
  #  locust --slave --no-web $protocols
  #done
#done