import os
import subprocess
import time


class Service(object):
    """
    Start/Stop Service with gradle
    """

    command = 'run'

    def __init__(self, path):
        super().__init__()
        self.path = path
        if os.path.isdir(path):
            gradle_build = os.path.join(path, "build.gradle")
            gradle_exec = os.path.join(path, "gradlew")
            if os.path.isfile(gradle_build) and os.path.isfile(gradle_exec):
                self.args = [gradle_exec, "-p " + path, Service.command]
                self.process = None
            else:
                raise FileNotFoundError("build.gradle or/and gradlew not found")
        else:
            raise OSError("no directory or no permission to read")

    def start(self):
        logfile = os.path.join("results", os.path.basename(self.path))
        if not os.path.isfile(logfile):
            self.log = open(logfile, "w")
            cmd = ' '.join(self.args)
            self.process = subprocess.Popen(cmd, stdout=self.log, shell=True, close_fds=True)
            time.sleep(5)  # wait till Service is up and running
            if self.process.poll() is not None:
                self.log.close()
                raise RuntimeError("Error when starting service: " + cmd)
        else:
            print(self.path + " is already running")

    def stop(self):
        try:
            self.process.terminate()
        except Exception as e:
            pass
