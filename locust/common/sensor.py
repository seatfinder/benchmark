import random
from datetime import datetime

import pytz


class Sensor(object):
    timezone = pytz.timezone('Europe/Zurich')

    def measurement(self):
        return {"dateTime": datetime.now(tz=Sensor.timezone), "value": random.randint(0, 1000)}